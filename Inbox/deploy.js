const HDWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const { interface, bytecode } = require('./compile');

const provider = new HDWalletProvider(
	'honey pilot loop extra fat crew find wink possible input midnight loyal', 
	'https://rinkeby.infura.io/8z3gJ5JDh39SRpIalFi6'
);

const web3 = new Web3(provider);

const deploy = async () => {
	const accounts = await web3.eth.getAccounts();
	console.log('Загружаю контракт с адреса', accounts[0]);
	const result = await new web3.eth.Contract(JSON.parse(interface))
		.deploy({ data: bytecode, arguments: ['Привет!'] })
		.send({ gas: '1000000', from: accounts[0] });
	console.log('Адрес контракта', result.options.address);
};
deploy();