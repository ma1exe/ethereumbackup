const path = require('path'); // подключаем модуль path
const fs = require('fs'); // подключаем модуль fs
const solc = require('solc'); // подключаем модуль компилятора

const inboxPath = path.resolve(__dirname, 'contracts', 'Inbox.sol'); // определяем полный путь до контракта
const source = fs.readFileSync(inboxPath, 'utf8'); // считываем исходный код контракта

module.exports = solc.compile(source, 1).contracts[':Inbox']; // экспортируем в глобальный объект скомпилированный объект контракта
