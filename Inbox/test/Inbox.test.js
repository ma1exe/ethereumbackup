const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');

const web3 = new Web3(ganache.provider());

const { interface, bytecode } = require('../compile');

let accounts;
let inbox;

beforeEach(async () => {
	// Получаем список всех аккаунтов
	accounts = await web3.eth.getAccounts();

	// Используем один из аккаунтов для загрузки в сеть контракта
	inbox = await new web3.eth.Contract(JSON.parse(interface))
			.deploy({ data: bytecode, arguments: ['Привет!'] })
			.send({ from: accounts[0], gas: '1000000' });
});

describe('Inbox', () => {

	it('Загрузка контракта', () => {
		assert.ok(inbox.options.address);
	});

	it('Приветствие добавлено', async () => {
		const message = await inbox.methods.message().call();
		assert.equal(message, 'Привет!');
	});

	it('setMessage обновляет значение message', async () => {
		await inbox.methods.setMessage('Пока!')
				.send({ from: accounts[0] });
		const message = await inbox.methods.message().call();
		assert.equal(message, 'Пока!');
	});

});