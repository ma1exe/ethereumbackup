import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import web3 from './web3'
import lottery from './lottery';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      manager: '0x0',
      players: [],
      balance: '0',
      value: '0',
      status: ''
    };
  }

  async componentDidMount() {
    const manager = await lottery.methods.manager().call();
    const players = await lottery.methods.getPlayers().call();
    const balance = await web3.eth.getBalance(lottery.options.address);

    this.setState({manager, players, balance});
  }

  enter = async (event) => {
    event.preventDefault();
    const accounts = await web3.eth.getAccounts();
    this.setState({ status: 'Ожидаем успешной отправки транзакции...' });
    await lottery.methods.enter().send({
      from: accounts[0],
      value: web3.utils.toWei(this.state.value, 'ether')
    });
    this.setState({ status: 'Поздравляем! Вы стали участником лотереи.' });
  }

  pickWinner = async () => {
    const accounts = await web3.eth.getAccounts();
    this.setState({ status: 'Ожидаем успешной отправки транзакции...' });
    await lottery.methods.pickWinner().send({
      from: accounts[0]
    });
    this.setState({ status: 'Победитель выбран.' });
  }

  render() {
    return (
      <div>
        <h2>Лотерея</h2>
        <p>Адрес управляющего контрактом {this.state.manager}</p>
        <p>Текущее количество участников лотереи: {this.state.players.length} чел.</p>
        <p>Призовой фонд: {web3.utils.fromWei(this.state.balance, 'ether')} ETH</p>
        <hr />
        <form onSubmit={this.enter}>
          <h3>Хотите попробовать?</h3>
          <div>
            <label>Количество эфира</label>
            <input
              value={this.state.value}
              onChange={event => this.setState({ value: event.target.value })}
            />
          </div>
          <button>Принять участие</button>
        </form>
        <hr />
        <h3>Время выбрать победителя?</h3>
        <button onClick={this.pickWinner}>Выбрать победителя</button>
        <hr />
        <h3>{this.state.status}</h3>
      </div>
    );
  }
}

export default App;